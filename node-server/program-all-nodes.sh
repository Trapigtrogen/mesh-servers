#!/bin/bash

FILE_TO_UPLOAD="./node-server.ino"

arduino-cli compile --fqbn esp8266:esp8266:d1_mini "$FILE_TO_UPLOAD" &&

for CUR_PORT in $(find /dev/ttyUSB*)
do
    arduino-cli upload --fqbn esp8266:esp8266:d1_mini "$FILE_TO_UPLOAD" --port "$CUR_PORT" --verify
done
