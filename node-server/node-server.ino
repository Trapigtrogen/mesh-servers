#include <ArduinoJson.h>
#include <ArduinoWebsockets.h>
#include <painlessMesh.h>
#include <ESP8266WiFi.h>

bool connected = false;

// WiFi settings for debug wifi
const char* ssid = "ssid"; //Enter SSID
const char* password = "password"; //Enter Password

// Mesh settings
#define   MESH_PREFIX     "ESP-testiverkko"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5000

// Websocket server settings
#define   SERVER_IP       "10.52.171.200"
#define   SERVER_PORT     8080

#define	  WEBSOCKETS_TCP_TIMEOUT 1;

// Sensor pin: Happened to have some potentiometers so using those as mock
int sensorPin = A0;

// Websocket mode to async
#define WEBSOCKETS_NETWORK_TYPE = NETWORK_ESP8266_ASYNC
websockets::WebsocketsClient ws;

// PainlessMesh
Scheduler userScheduler; // to control your personal task
painlessMesh  mesh;

// User stub
void ProcessNode();
Task nodeProcessTask( TASK_SECOND * 1 , TASK_FOREVER, &ProcessNode );
void WSSendSensorData(String msg = "");

// Processes data and sends message to other nodes
void ProcessNode() {
	String msg = CreateDataMessage();
	mesh.sendBroadcast( msg );
	nodeProcessTask.setInterval( random( TASK_SECOND * 1, TASK_SECOND * 5 ));
}

// Needed for painless library
void receivedCallback( uint32_t from, String &msg ) {
	digitalWrite(LED_BUILTIN, LOW); // Blink led to know there's data moving
	Serial.printf("Received from %u msg=%s\n", from, msg.c_str());
	WSSendSensorData(msg);
	digitalWrite(LED_BUILTIN, HIGH);
}

void newConnectionCallback(uint32_t nodeId) {
	Serial.printf("--> New Connection, nodeId = %u\n", nodeId);
}

void changedConnectionCallback() {
	Serial.printf("Changed connections\n");
}

void nodeTimeAdjustedCallback(int32_t offset) {
	Serial.printf("Adjusted time %u. Offset = %d\n", mesh.getNodeTime(),offset);
}

void setupMesh() {
	//mesh.setDebugMsgTypes( ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE ); // all types on
	mesh.setDebugMsgTypes( ERROR | STARTUP );  // set before init() so that you can see startup messages

	mesh.init( MESH_PREFIX, MESH_PASSWORD, &userScheduler, MESH_PORT );
	mesh.onReceive(&receivedCallback);
	mesh.onNewConnection(&newConnectionCallback);
	mesh.onChangedConnections(&changedConnectionCallback);
	mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);

	userScheduler.addTask( nodeProcessTask );
	nodeProcessTask.enable();
}

// DEBUG Function for direct connections
int connectWifi() {
	// Connect to wifi
	WiFi.begin(ssid, password);

	// Wait some time to connect to wifi
	for(int i = 0; i < 10 && WiFi.status() != WL_CONNECTED; i++) {
		Serial.print(".");
		delay(1000);
	}

	// Check if connected to wifi
	if(WiFi.status() != WL_CONNECTED) {
		Serial.println("No Wifi!");
		return -1;
	}

	Serial.println("Connected to Wifi, Connecting to server.");
}

String CreateDataMessage() {
	String msg = "{\"creationTime\":" + String(mesh.getNodeTime()) + ",\"data\":[";
	msg += String(analogRead(sensorPin));
	msg += "],\"nodeId\":";
	msg += String(mesh.getNodeId());
	msg += ", \"messageType\":0";
	msg += ", \"header\":\"Potentiometer value\",\"sensorId\":1}";

	return msg;
}

String CreateTextMessage(String txt) {
	String msg = "{\"creationTime\":" + String(mesh.getNodeTime());
	msg += ",\"nodeId\":";
	msg += String(mesh.getNodeId());
	msg += ", \"messageType\":1";
	msg += ", \"header\":\"";
	msg += txt;
	msg += "\"}";

	return msg;
}

// Tries to send the given message to websocket
// If message is not given generate own
void WSSendSensorData(String msg) {
	if(!connected) {
		if (connectToWebSocket() == -1 ) {
			return;
		}
	}

	if(msg.isEmpty()) {
		msg = CreateDataMessage();
	}
	ws.send(msg);
}

int connectToWebSocket() {
	Serial.println("Trying to connect to the websocket");
	connected = ws.connect(SERVER_IP, SERVER_PORT, "ws://");
	if(connected) {
		Serial.println("Connected");
		return 0;
	}
	else {
		Serial.println("Didn't connect");
		return -1;
	}
}

void updateWebSocket(){
	if(ws.available()) {
		Serial.println("Syncing with websocket...");
		// Let the websockets ws check for incoming messages
		ws.poll();
		WSSendSensorData();
	}
	else connected = false;
}

// Gather information from the nodes and send your own
// it will run the user scheduler as well
void updateMesh() {
	mesh.update();
}

void setup() {
	Serial.begin(115200);
	pinMode(sensorPin, INPUT);
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWrite(LED_BUILTIN, LOW);

	// Create mesh network
	setupMesh();

	// DEBUG: Replace setupMesh() for testing websockets without the mesh
	//connectWifi();

	// run callback when messages are received from master server
	ws.onMessage([&](websockets::WebsocketsMessage message) {
		Serial.print("Got Message: ");
		Serial.println(message.data());
	});

	ws.setInsecure();

	digitalWrite(LED_BUILTIN, HIGH);
}

// Timers
int prevMillis = 0;
int timerWS = 0;
// To make more failed rerties result longer waiting between
int wsTries = 0;
void loop() {
	updateMesh();

	// WebScoket timer
	timerWS += (millis() - prevMillis);
	prevMillis = millis();

	if(connected && timerWS > 100) {
		timerWS = 0;
		updateWebSocket();

		// DEBUG for sending text-only message (no datavalue in json)
		String msg = CreateTextMessage("Random text-only message");
		ws.send(msg);
		// END OF DEBUG
	}
	else if (!connected && timerWS > 10000 + wsTries * 1000) {
		timerWS = 0;
		wsTries++;
		if( connectToWebSocket() ) {
			// Reset timer so it doesn't connect again right away
			prevMillis = millis();
		}
	}
}