#ifndef MESH_SERVER_MESSAGE
#define MESH_SERVER_MESSAGE

#include <bits/types/time_t.h>
#include <cstdio>
#include <ctime>

#include <vector>
#include "json.hpp"

namespace Mesh {

	class Message {
	public:
		enum MessageType {
			dataMessage,
			textMessage
		};

		Message(char* _header, long _nodeId, int _sensorId, std::vector<int> &_data, MessageType _messageType = MessageType::dataMessage) {
			nodeId = _nodeId;
			sensorId = _sensorId;
			header = _header;
			creationTime = std::time(0);
			data = _data;
			messageType = _messageType;
		}

		Message(char* _header, long _nodeId, int _sensorId, std::vector<int> &_data, time_t _creationTime, MessageType _messageType = MessageType::dataMessage) {
			nodeId = _nodeId;
			sensorId = _sensorId;
			header = _header;
			creationTime = _creationTime;
			data = _data;
			messageType = _messageType;
		}

		MessageType messageType;
		long nodeId; // From which node the data comes from
		int sensorId; // From which sensor the data comes from
		char* header;
		std::vector<int> data;
		time_t creationTime; // TODO can this be used as arduino cannot get time. Maybe get system time and add mesh uptime. Or just mesh uptime

		nlohmann::json getAsJson() {
			nlohmann::json j;
			j["header"] = header;
			j["nodeId"] = nodeId;
			j["sensorId"] = sensorId;
			j["data"] = data;
			j["creationTime"] = creationTime;

			return j;
		}
	};

} // namespace Mesh

#endif // MESH_SERVER_MESSAGE