#ifndef MESH_SERVER_CONNECTION
#define MESH_SERVER_CONNECTION

#include <vector>

// Websocket libraries
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>

// async
#include <thread>
#include <mutex>
#include <chrono> // Timing for sleep

#include "message.h"

#include <tiny_websockets/server.hpp>
#include <iostream>
#include <cstring>

namespace Mesh {

	class Connection {
	public:
		Connection(websockets::WebsocketsClient _client);

		long nodeId = 0;

		websockets::WebsocketsClient client;

		// Mark the connection to be killed by ConnectionCleanup thread
		void MarkDead();

		// Called for each connection thread. Thread's main loop
		static void ProcessConnection(Mesh::Connection* connection,
						std::vector<std::thread*> threads,
						std::vector<Mesh::Connection*> connections);

		bool shouldJoin = false;
		std::thread::id threadId;

		long lastMessageTime = 0; // in mesh time
	private:
		Message ReceiveMessage();

		bool continueProcessing = true;
	};

} // namespace Mesh

#endif // MESH_SERVER_CONNECTION