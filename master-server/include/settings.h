#ifndef MESH_SERVER_SETTINGS_H
#define MESH_SERVER_SETTINGS_H
#include <stdint.h>
#include <vector>
#include <thread>

#include "connection.h"


#define PORT 8080

// =-=-= TIMING =-=-=-=-=-=-=-=-=-=-=-=-=

// How many empty messages until connection is marked dead
const int TIMEOUT = 10;

// server lookup polling rate (milli sec)
const int POLL_RATE = 100;

// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

#endif // MESH_SERVER_SETTINGS_H