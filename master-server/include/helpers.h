#ifndef MESH_SERVER_HELPERS
#define MESH_SERVER_HELPERS

#include <string.h>
#include <iostream>

namespace Mesh {

	class Helpers {
	public:
		static void reverse(char str[], int length) {
			int start = 0;
			int end = length -1;
			while (start < end)
			{
				std::swap(*(str+start), *(str+end));
				start++;
				end--;
			}
		}

		static char* itoa(int num, char* str, int base) {
			int i = 0;
			bool isNegative = false;

			/* Handle 0 explicitly, otherwise empty string is printed for 0 */
			if (num == 0)
			{
				str[i++] = '0';
				str[i] = '\0';
				return str;
			}

			// In standard itoa(), negative numbers are handled only with
			// base 10. Otherwise numbers are considered unsigned.
			if (num < 0 && base == 10)
			{
				isNegative = true;
				num = -num;
			}

			// Process individual digits
			while (num != 0)
			{
				int rem = num % base;
				str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
				num = num/base;
			}

			// If number is negative, append '-'
			if (isNegative)
				str[i++] = '-';

			str[i] = '\0'; // Append string terminator

			// Reverse the string
			reverse(str, i);

			return str;
		}

		// Some prime numbers
		#define A 54059
		#define B 76963
		#define C 86969
		#define FIRSTH 37
		static unsigned int hash_str(const char* s) {
			unsigned h = FIRSTH;
			while (*s) {
				h = (h * A) ^ (s[0] * B);
				s++;
			}
			return h; // or return h % C;
		}

		// Process given command line arguments
		static int ProcessArguments(int argc, const char* argv[]) {
		for (int i = 1; i < argc; i+=2) {
			printf("arg: %s, value: %s\n", argv[i], argv[i+1]);

			// Example
			// if(strcmp(argv[i], "--arg") == 0) {
			//     value = argv[i+1];
			// }
		}
			// OK
			return 0;
		}
	};

} // namespace Mesh

#endif // MESH_SERVER_HELPERS