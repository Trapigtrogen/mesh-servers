#include <cstddef>
#include <cstring>
#include <ratio>
#include <stdio.h>
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <unistd.h>
#include <vector>
#include <functional>
#include <csignal>

// Server settings in one place
#include "settings.h"
#include "helpers.h" // Nothing used anymore
#include "message.h"
#include "connection.h"

std::vector<Mesh::Connection*> connections;
std::vector<std::thread*> threads;
bool continueRunning = true;
long lastMessageTime = 0; // in mesh time


void signalHandler(int signum) {
	printf("signal: %i\n", signum);

	switch (signum) {
		case 2:
			printf("User interrupted: Exiting\n");
			continueRunning = false;
			for(int i = 0; i > connections.size(); i++) {
				connections.at(i)->MarkDead();
			}
			exit(signum);
		break;

		default:
		break;
	}
}

// Separate thread that removes dead connections from memory
void ConnectionCleanup() {
	while (continueRunning) {
		sleep(10); // Go through connections every 10 sec
		for(int con = 0; con < connections.size(); con++) {
			// Kill connections when last message has seen over 10 sec ago
			if(lastMessageTime-connections.at(con)->lastMessageTime > 10000000) {
				connections.at(con)->MarkDead();
			}
			if(connections.at(con)->shouldJoin) {
				// Join thread, free it from memory and remove from vector
				for(int thr = 0; thr < threads.size(); thr++) {
					if(threads.at(thr)->get_id() == connections.at(con)->threadId) {
						printf("Connection killed. Joining thread and cleaning data\n");
						threads.at(thr)->join();
						free(threads.at(thr));
						threads.erase(threads.begin() + thr);
					}
				}
				// free connection from memory and remove from vector
				free(connections.at(con));
				connections.erase(connections.begin() + con);
			}
		}
	}
}

int main(int argc, const char* argv[]) {
	// Set signalHandler as interrupt function
	signal(SIGINT, signalHandler);

	// separate thread that cleans up dead connections
	std::thread conMan(ConnectionCleanup);

	// Process given arguments (nothing used atm)
	//if(Mesh::Helpers::ProcessArguments(argc, argv) != 0) return -1;

	// Create websocket
	websockets::WebsocketsServer server;
	server.listen(8080);
	if(!server.available()) {
		printf("Server not available\n");
		exit(-1);
	}
	printf("Server up and running\n");

	// Accept new clients
	while(continueRunning) {
		websockets::WebsocketsClient client = server.accept();
		Mesh::Connection* connection = new Mesh::Connection(client);
		connection->client.send((char*)"conencted");

		connections.push_back(connection);
		// Create new processing thread for each client
		std::thread* connectionProcessor = new std::thread(Mesh::Connection::ProcessConnection, connection, threads, connections);
		threads.push_back(connectionProcessor);

		printf("Client connected\n");
	}

	// Cleanup
	conMan.join();

	return 0;
}