#include "connection.h"
#include "json.hpp"
#include "message.h"
#include "settings.h"
#include "helpers.h"
#include "tiny_websockets/message.hpp"
#include <bits/types/time_t.h>
#include <cstdio>
#include <cstring>
#include <vector>

namespace Mesh {

	Connection::Connection(websockets::WebsocketsClient _client) {
		client = _client;
	}

	Message Connection::ReceiveMessage() {
		std::vector<int> data;

		if(!client.available()) {
			printf("Client not available\n");
			return Message(NULL, 0, 0, data, 0);
		}
		// Check if there's new messages
		if(!client.poll()) {
			return Message(NULL, 0, 0, data, 0);
		}
		// Read new message
		websockets::WebsocketsMessage message = client.readBlocking();
		if(message.isEmpty()) {
			return Message(NULL, 0, 0, data, 0);
		}

		char* cmessage = new char[message.data().size()+1];
		std::strcpy(cmessage, message.data().c_str());

		printf("DEBUG: cmessage: %s\n\n", cmessage);


		nlohmann::json j = nlohmann::json::parse(cmessage);

		long nodeId = j["nodeId"].get<long>();
		time_t time = j["creationTime"].get<time_t>();
		int messageType = j["messageType"].get<int>(); // TODO map to MessageType enum
		Mesh::Message::MessageType type = Message::textMessage;
		int sensorId  = -1;

		if(messageType == 0) { // TODO use enum
			type = Message::dataMessage;
			data = j["data"].get<std::vector<int>>();
			sensorId = j["sensorId"].get<int>();
		}

		// Convert header to char*
		std::string header = j["header"].get<std::string>();
		char* cstrheader = new char[header.size()+1];
		std::strcpy( cstrheader, header.c_str() );

		return Message(cstrheader, nodeId, sensorId, data, time, type);
	}

	void Connection::ProcessConnection(Connection* connection, std::vector<std::thread*> threads,  std::vector<Connection*> connections) {
		connection->threadId = std::this_thread::get_id();

		int emptyMessages = 0;
		while(connection->continueProcessing) {
			printf("Processing connection: %lu\n", connection->nodeId);

			// READ
			Message msg( connection->ReceiveMessage() );

			if( msg.header != NULL ) {
				emptyMessages = 0;
				connection->nodeId = msg.nodeId;

				printf("Header: %s\n", msg.header);
				printf("nodeId: %lu\n", msg.nodeId);
				if(msg.messageType == Message::dataMessage) {
					printf("sensorId: %i\n", msg.sensorId);
					printf("Data: ");
					for(int i = 0; i < msg.data.size(); i++) {
						printf("%i, ", msg.data.at(i));
					}
					printf("\n");
				}
				printf("creationTime: %lu\n\n", msg.creationTime);
			}
			else {
				emptyMessages++;
			}

			if(emptyMessages >= TIMEOUT) {
				printf("Too many empty messages. Disconnected\n");
				connection->MarkDead();
			}

			if(msg.creationTime > connection->lastMessageTime) {
				connection->lastMessageTime = msg.creationTime;
			}

			// SEND
			// TODO sending message if needed

			// TODO log message


			std::this_thread::sleep_for( std::chrono::milliseconds(POLL_RATE) );
		}
	}

	void Connection::MarkDead() {
		shouldJoin = true;
		continueProcessing = false;
	}

} // namespace Mesh