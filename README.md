Data servers for mesh network.\
Support for x86 and ARM devices running Linux.\
Node software is built for esp8266. More specifically using WeMos D1 Mini.

## Build Requirements
You'll need to install the following packages:
### For Debian based
- libssl-dev cmake gcc

### For Arch based
- base-devel cmake

## Node software
I'm using arduino-cli because it can compile the sketch once and upload it to all nodes.\
Before using the program-all-nodes.sh script make sure that you have installed and configured arduino-cli.\
Arduino IDE works normally.\

Libraries used:
- PainlessMesh (Edwin van Leeuwen)
- ArduinoJson (Benoit Blancon)
- ArduinoWebsockets (Gil Maimon)

## Build master-server
inside cloned repository:
```
$ mkdir build
$ cd build
$ cmake ..
$ make
```
one liner: `mkdir build && cd build && cmake .. && make`

## Setup PI
Using Raspberry OS it's easiest to connect to internet via ethernet and edit `/etc/dhcpcd.conf` to  set static ip for eth0 from the example and then editing `/etc/wpa_supplicant/wpa_supplicant.conf` to set the internet by adding / replacing so there's only one network:
```
network={
        ssid="mesh-network-name"
        psk="mesh-network-password"
}
```

I tried to connect via `sudo raspi-config` but the wifi fell back to the one with internet connection. Maybe it'll work if not connected to other networks before.
